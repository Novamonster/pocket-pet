#!/usr/bin/env python

class Enemy(object):
    """The enemy the Critter has to face off agaisnt"""
    def __init__(self, Critter, difficulty):
        super(Enemy, self).__init__()
        self.critter = Critter
        self.difficulty = difficulty
        self.decideEnemy()

    def decideEnemy(self):
        '''Chooses enemy stats depending on difficulty'''
        if self.difficulty == 0:
            self.attack = round(1 * (self.critter.base_atk))
            self.hp = (self.critter.base_hp) * 6
            self.basehp = self.hp
            self.name = "Slime"
            self.image = "../Assets/Images/Enemies/Slime.png"
            
        elif self.difficulty == 1:
            self.attack = round(1.5 * (self.critter.base_atk))
            self.hp = (self.critter.base_hp) * 12
            self.basehp = self.hp
            self.name = "Orc"
            self.image = "../Assets/Images/Enemies/Orc.png"
            
        elif self.difficulty == 2:
            self.attack = round(3 * (self.critter.base_atk))
            self.hp = (self.critter.base_hp) * 20
            self.basehp = self.hp
            self.name = "Dragon"
            self.image = "../Assets/Images/Enemies/Dragon.png"
            
        else:
            self.attack = round(1 * (self.critter.base_atk))
            self.hp = (self.critter.base_hp) * 6
            self.basehp = self.hp
            self.name = "Slime"
            self.image = "../Assets/Images/Enemies/Slime.png"
            
        '''not needed '''
        #decideEnemy.self.attack = self.attack
        #decideEnemy.self.hp = self.hp
        #decideEnemy.basehp = basehp


    def update(self):
        pass

    def on_touch_move(self, touch):
        attack_animation1()
        while self.decideEnemy.hp > 0:
            self.Enemy.hp = Enemy.hp - Critter.attack
        endEnemy.hp = self.Enemy.hp
        return

    def on_touch_down(self, touch):
        attack_animation1()
        while self.decideEnemy.hp > 0:
            self.Enemy.hp = Enemy.hp - Critter.attack
        endEnemy.hp = self.Enemy.hp
        return

    def on_touch_up(self, touch):
        attack_animation2()
        return
