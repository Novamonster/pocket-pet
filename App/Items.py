#!/usr/bin/env python

class Item(object):
    pass
# temp attack items
class LowLevelElixer(Item):
    name = "lowLevelElixer"
    value = 50
    tmp_hp = 0
    tmp_atk = 4
    mod_atk = 0
    mod_hp = 0

    def __init__(self):
        super(LowLevelElixer, self).__init__()
        
class MidLevelElixer(Item):
    name = "midLevelElixer"
    value = 100
    tmp_hp = 0
    tmp_atk = 8
    mod_atk = 0
    mod_hp = 0

    def __init__(self):
        super(MidLevelElixer, self).__init__()
        
class HighLevelElixer(Item):
    name = "highLevelElixer"
    value = 150
    tmp_hp = 0
    tmp_atk = 16
    mod_atk = 0
    mod_hp = 0
    def __init__(self):
        super(HighLevelElixer, self).__init__()
        
# name = {'lowLevelElixir': 1, 'midLevelElixir':2, 'highLevelElixir':3}

# temp hp boosts
class CannedFood(Item):
    name = "CannedFood"
    value = 50
    tmp_hp = 4
    tmp_atk = 0
    mod_atk = 0
    mod_hp = 0
    def __init__(self):
        super(CannedFood, self).__init__()
        

class Beans(Item):
    name = "Beans"
    value = 100
    tmp_hp = 8
    tmp_atk = 0
    mod_atk = 0
    mod_hp = 0
    def __init__(self):
        super(Beans, self).__init__()
        

class SpaghettiDinner(Item):
    name = "SpaghettiDinner"
    value = 150
    tmp_hp = 16
    tmp_atk = 0
    mod_atk = 0
    mod_hp = 0
    def __init__(self):
        super(SpaghettiDinner, self).__init__()
        
# name = {'cannedFood': 1, 'beans':2, 'spaghettiDinner':3}

#permanent attack boosts

class SmallWeapon(Item):
    name = "SmallWeapon"
    value = 100
    tmp_hp = 0
    tmp_atk = 0
    mod_atk = 1
    mod_hp = 0
    def __init__(self):
        super(SmallWeapon, self).__init__()
        

class GeniusStrategy(Item):
    name = "GeniusStrategy"
    value = 200
    tmp_hp = 0
    tmp_atk = 0
    mod_atk = 2
    mod_hp = 0
    def __init__(self):
        super(GeniusStrategy, self).__init__()
        

class BearArms(Item):
    name = "BearArms"
    value = 300
    tmp_hp = 0
    tmp_atk = 0
    mod_atk = 3
    mod_hp = 0
    def __init__(self):
        super(BearArms, self).__init__()
        
# name = {'smallWeapon': 1, 'geniusStrategy':2, 'bearArms':3}

# permanent hp boosts

class Bandaids(Item):
    name = "Bandaids"
    value = 100
    tmp_hp = 0
    tmp_atk = 0
    mod_atk = 0
    mod_hp = 1
    def __init__(self):
        super(Bandaids, self).__init__()
        

class SuitOfArmor(Item):
    name = "SuitOfArmor"
    value = 200
    tmp_hp = 0
    tmp_atk = 0
    mod_atk = 0
    mod_hp = 2
    def __init__(self):
        super(SuitOfArmor, self).__init__()
        

class SpontaneousRegenerationAbility(Item):
    name = "SpontaneousRegenerationAbility"
    value = 300
    tmp_hp = 0
    tmp_atk = 0
    mod_atk = 0
    mod_hp = 3
    def __init__(self):
        super(SpontaneousRegenerationAbility, self).__init__()
        
# name = {'bandaids': 1, 'suitOfArmor':2, 'spontaneousRegenerationAbility':3}
