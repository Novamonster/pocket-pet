#!/usr/bin/env python

from kivy.core.audio import SoundLoader

class SoundBoard(object):
	"""SoundBoard is a Kivy-oriented controller for interacting with a given library of sounds"""

	def __init__(self, library, *args, **kwargs):
		super(SoundBoard, self).__init__()
		self.library = library
		self.sound = 0
		self.effect = 0
		self.loopsound = 0
		self.persist = 0

	def playSound(self, tune):
		self.sound = SoundLoader.load(self.library[tune])
		self.sound.play()

	def playEffect(self, tune):
		self.effect = SoundLoader.load(self.library[tune])
		self.effect.play()

	def loopSound(self, tune):
		if self.persist > 0:
			self.persist = 0
			return
		self.loopsound = SoundLoader.load(self.library[tune])
		self.loopsound.loop = True
		self.loopsound.play()

	def stopSound(self):
		self.sound.stop()

	def stopLoopSound(self):
		self.loopsound.stop()

	#in critter.kv
	#root.manager.soundboard.playSound(tune="house")
