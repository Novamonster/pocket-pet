#!/usr/bin/env python3

##Imports --Py:
import re
from functools import wraps
from typing import Dict, List, Tuple, Union

##Imports --Internal:
from pyToKivy import PythonToKivy

##Imports --Kivy:
import kivy
kivy.require('1.11.1')

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.properties import DictProperty, ListProperty, NumericProperty, StringProperty
from kivy.event import EventDispatcher
from kivy.lang import Builder


###################
# Sample Model Data
###################

class DataModel(object):
    def __init__(self):
        self.a = 'This is a'
        self.b = 'This is b'
        self.d = 5
        self.e = 5.5
        self.f = ["pinggo"]
        self.g = {"apple":5}
        self.h = (1, "a")

    @property
    def c(self):
        return self.a + '\nand\n' + self.b

    def incrementD(self):
        self.d += 1

###########################
# Sample App with Changes
###########################

Builder.load_string("""
<RootWidget>:
    rows: 5
    cols: 2
    Label:
        text: "Attribute a:"
    Label:
        text: root.kivyObj.a
    Label:
        text: "Attribute b:"
    Label:
        text: root.kivyObj.b
    Label:
        text: "Attribute c:"
    Label:
        text: str(root.kivyObj.c)
    Label:
        text: "Attribute d:"
    Label:
        text: str(root.kivyObj.d)
    Button:
        text: "Modify A text directly!"
        on_press: root.button_press()
    Button:
        text: "Add some to D."
        on_press: root.button_press2()
""")
        
class RootWidget(GridLayout):

    customPyObj = DataModel()
    tfm = PythonToKivy()
    kivyObj = tfm(customPyObj)

    def __init__(self):
        super(RootWidget, self).__init__()

    ##Test functions to modify the Kivy UI object:
    def button_press(self, *args):
        self.kivyObj.data_model.a = 'This is a and it is really long now'
        self.kivyObj.a = self.kivyObj.data_model.a
        print(self.kivyObj.data_model.a)

    def button_press2(self, *args):
        self.kivyObj.incrementD()
        self.kivyObj.d = self.kivyObj.data_model.d

class TestApp(App):
    def build(self):
        return RootWidget()

def main():
    app = TestApp()
    app.run()

if __name__ == '__main__':
    main()