#!/usr/bin/env python

##To run for tests:
# python3 main.py -m screen:phone_iphone_6 #,portrait

##Imports -- Kivy
import kivy
from kivy.app import App

from kivy.uix.gridlayout import GridLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget

from kivy.graphics import Ellipse

from kivy.core.audio import SoundLoader
from kivy.core.window import Window

from kivy.lang import Builder
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty

from kivy.vector import Vector
from kivy.clock import Clock

##Imports -- Py
import gc
import json
import os
import time

##Imports -- Project
from Critters import Foxy, Snek, Birb
from Players import Player
from Enemies import Enemy
from Competitions import Competition
from SoundBoards import SoundBoard
from pyToKivy import PythonToKivy
from sound_library import library

##Configuration
kivy.require("1.11.1")
# os.environ['KIVY_TEXT'] = 'pil'
# os.environ["KCFG_KIVY_LOG_LEVEL"] = "warning"

##Hybrid Use Classes:
class ImageButton(ButtonBehavior, Image):
    pass

class NameInput(TextInput):

    def insert_text(self, substring, from_undo=True):
        s = str(substring)
        if len(s)>12:
            s = s[:12]
        return super(NameInput, self).insert_text(s, from_undo=from_undo)

##Screens:
class Menu(Screen):
    def on_enter(self):
        self.manager.soundboard.loopSound(tune="mainmenu")
        pass

class Inventory(Screen):

    def on_pre_enter(self):
        self.update_labels()
        pass

    def update_labels(self):
        self.ids.cannedfoodlabel.text = str(self.manager.player.data_model.inventory["0"][-1])
        self.ids.beanslabel.text = str(self.manager.player.data_model.inventory["1"][-1])
        self.ids.spaghettidinnerlabel.text = str(self.manager.player.data_model.inventory["2"][-1])
        self.ids.lowlevelelixerlabel.text = str(self.manager.player.data_model.inventory["3"][-1])
        self.ids.midlevelelixerlabel.text = str(self.manager.player.data_model.inventory["4"][-1])
        self.ids.highlevelelixerlabel.text = str(self.manager.player.data_model.inventory["5"][-1])
        self.ids.smallweaponlabel.text = str(self.manager.player.data_model.inventory["6"][-1])
        self.ids.geniusstrategylabel.text = str(self.manager.player.data_model.inventory["7"][-1])
        self.ids.beararmslabel.text = str(self.manager.player.data_model.inventory["8"][-1])
        self.ids.bandaidslabel.text = str(self.manager.player.data_model.inventory["9"][-1])
        self.ids.suitofarmorlabel.text = str(self.manager.player.data_model.inventory["10"][-1])
        self.ids.spontaneousregenerationlabel.text = str(self.manager.player.data_model.inventory["11"][-1])
        pass 
    
class Explore(Screen):
    def __init__(self, *args, **kwargs):
        super(Explore, self).__init__()
        self.tap_counter = 0
    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        self.plot = Image(source='../Assets/Images/Effects/Splash_Blue_Transparent.png')
        self.manager.soundboard.playSound(tune="critterpunch")
      # plot.pos_hint['x'] =  0.1*plot.size_hint_x
      # plot.pos_hint['y'] =  0.1*plot.size_hint_y
        self.plot.pos = (touch.x - 35, touch.y - 30)
        self.plot.size_hint = .1, .1
        #print(touch.x, touch.y)
        self.add_widget(self.plot)

    def on_touch_up(self, touch):
        self.remove_widget(self.plot)

    def on_pre_enter(self, *args):
        self.ids.enemy_name.text = self.manager.competition.Enemy.name
        self.ids.enemy_image.source = self.manager.competition.Enemy.image
        self.ids.critter_image2.source = self.manager.critter.data_model.imgpath
        self.ids.enemy_health.text = str(self.manager.competition.data_model.Enemy.hp)
        self.ids.critter_health.text = str(self.manager.competition.data_model.critter.hp)
        self.ids.critter_attack.text = str(self.manager.competition.data_model.critter.atk)

    def on_enter(self, *args):
        if self.tap_counter == 0:
            self.tap = Label(text= "Tap the Monster!", font_size= 30)
            self.tap.pos = (0, 45)
            self.add_widget(self.tap)
            Clock.schedule_interval(lambda dt: self.remove_widget(self.tap), 2)
            self.tap_counter += 1
        self.timer = Clock.schedule_interval(lambda dt: self.pupdateTime(), 1)
        self.ids.timer_clock.text = str(self.manager.competition.data_model.t)

    def on_pre_leave(self, *args):
        pass

    def on_leave(self, *args):
        self.manager.critter.level = self.manager.critter.data_model.level
        self.manager.player.gold = self.manager.player.data_model.gold
        self.timer.cancel()
        try:
            self.remove_widget(self.plot)
        except:
            pass
        
    def pupdateTime(self):
        self.timerClock()
        self.ids.timer_clock.text = str(self.manager.competition.data_model.t)
        if self.manager.competition.data_model.enemy_counter==4:
            self.enemyStrong()
            self.manager.competition.data_model.enemy_counter=0
        self.manager.competition.data_model.enemy_counter += 1


    def timerClock(self):
        self.manager.competition.data_model.countdown()
        if self.manager.competition.data_model.t <= 0:
            self.manager.current = "defeatscreen"

    def enemyStrong(self):
        self.enemysplash = Image(source='../Assets/Images/Effects/Splash_Red_Transparent.png', pos_hint={'x':.44, 'y':.1})
        self.manager.soundboard.playSound(tune="enemypunch")
        self.enemysplash.pos = 1, 1
        self.enemysplash.size_hint = .1, .1
        self.add_widget(self.enemysplash)
        self.manager.competition.Enemyattack()
        self.ids.critter_health.text = str(self.manager.competition.data_model.critter.hp)
        if self.manager.competition.data_model.critter.hp <= 0:
            self.tap_counter = 0
            self.manager.current = "defeatscreen"
        Clock.schedule_interval(lambda dt: self.remove_widget(self.enemysplash), .3)

    def critterStrong(self):
        self.manager.competition.critterAttack()
        if self.manager.competition.data_model.crit == 1:
            self.critical = Label(text= "CRITICAL!", color= (100, 100, 0), font_size= 30)
            self.add_widget(self.critical)
            Clock.schedule_interval(lambda dt: self.remove_widget(self.critical), .3)

        self.ids.enemy_health.text = str(self.manager.competition.data_model.Enemy.hp)
        if self.manager.competition.data_model.Enemy.hp <= 0:
            self.manager.critter.data_model.tmp_hp = 99999999999999999999
            self.manager.critter.updateStats()
            self.tap_counter = 0
            self.manager.current = "victoryscreen"

#    def Critical(self):
#        if root.manager.competition.critterAttack.crit == 1: 
#            Clock.schedule_once(root.hide_widget(ide="critical", dohide=False), 1) 
#
#    def hide_widget(self, ide, dohide=True):
#        wid = self.ids[ide]
#        if hasattr(wid, 'saved_attrs'):
#            if not dohide:
#                wid.height, wid.size_hint_y, wid.opacity, wid.disabled = wid.saved_attrs
#                del wid.saved_attrs
#        elif dohide:
#            wid.saved_attrs = wid.height, wid.size_hint_y, wid.opacity, wid.disabled
#            wid.height, wid.size_hint_y, wid.opacity, wid.disabled = 0, None, 0, True

class Victoryscreen(Screen):
    def on_pre_enter(self, *args):
        self.manager.soundboard.stopLoopSound()
        self.victorySound()

    def on_enter(self, *args):
        self.manager.critter.competitionEnd()
        self.manager.critter.hp = self.manager.critter.data_model.hp
        self.manager.critter.atk = self.manager.critter.data_model.atk
        self.ids.victory.text = self.manager.competition.data_model.victory

    def victorySound(self):
        self.victory_tune = SoundLoader.load("../Assets/Sound/Victory.wav")
        if self.victory_tune:
            self.victory_tune.play()

    def on_pre_leave(self, *args):
        self.victory_tune.stop()

class Defeatscreen(Screen):
    def on_pre_enter(self, *args):
        self.manager.soundboard.stopLoopSound()
        self.defeatSound()

    def on_enter(self, *args):
        self.manager.critter.competitionEnd()
        #self.manager.competition.competitionFinish = self.manager.competition.data_model.competitionFinish
        self.manager.competition.competitionFinish2()
        self.manager.critter.hp = self.manager.critter.data_model.hp
        self.manager.critter.atk = self.manager.critter.data_model.atk
        self.ids.defeat.text = self.manager.competition.data_model.defeat

    def defeatSound(self):
        self.defeat_tune = SoundLoader.load("../Assets/Sound/Defeat.wav")
        if self.defeat_tune:
            self.defeat_tune.play()

    def on_pre_leave(self, *args):
        self.defeat_tune.stop()

class Quitcompetition(Screen):
    def cancelCompetition(self, *args):
        self.manager.competition.competitionFinish2()
        self.manager.critter.hp = self.manager.critter.data_model.hp
        self.manager.critter.atk = self.manager.critter.data_model.atk
       #Explore.enemyatkevent.cancel()
       #Explore.timer.cancel()
    pass

class Shop(Screen):
  # def on_pre_enter(self, *args):
  #     self.shopSound()
  #     pass

  # def shopSound(self):
  #     self.shop_tune = SoundLoader.load("../Assets/Sound/Shop2.mp3")
  #     if self.shop_tune:
  #         self.shop_tune.play()
  #     pass
  # 
  # def stopShop(self):
  #     self.shop_tune.stop()

    def on_enter(self):
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)

    def update(self):
        pass

    def purchase(self):
        print("Item purchased.")

    def on_touch_move(self, touch):
        return
    def on_pre_leave(self, *args):
        pass

class BoostHp(Screen):

    def on_enter(self):
        self.ids.valueOfCannedFood.text = "Cost: " + str(self.manager.player.inventory["0"][1].value)
        self.ids.valueOfBeans.text = "Cost: " + str(self.manager.player.inventory["1"][1].value)
        self.ids.valueOfMomsSpaghetti.text = "Cost: " + str(self.manager.player.inventory["2"][1].value)
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)

    def updateGold(self):
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)
        pass

    def purchase(self):
        print("Item purchased.")

    def on_touch_move(self, touch):
        return


class BoostAtk(Screen):

    def on_enter(self):
        self.ids.valueOfLowLevelElixir.text = "Cost: " + str(self.manager.player.inventory["3"][1].value)
        self.ids.valueOfMidLevelElixir.text = "Cost: " + str(self.manager.player.inventory["4"][1].value)
        self.ids.valueOfHighLevelElixir.text = "Cost: " + str(self.manager.player.inventory["5"][1].value)
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)


    def updateGold(self):
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)
        pass

    def purchase(self):
        print("Item purchased.")

    def on_touch_move(self, touch):
        return
    
class PermBoostHp(Screen):

    def on_enter(self):
        self.ids.valueOfBandaids.text = "Cost: " + str(self.manager.player.inventory["9"][1].value)
        self.ids.valueOfSuitOfArmor.text = "Cost: " + str(self.manager.player.inventory["10"][1].value)
        self.ids.valueOfSpontaneousRegeneration.text = "Cost: " + str(self.manager.player.inventory["11"][1].value)
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)


    def updateGold(self):
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)
        pass

    def purchase(self):
        print("Item purchased.")

    def on_touch_move(self, touch):
        return

class PermBoostAtk(Screen):

    item1 = ObjectProperty(None)

    def on_enter(self):
        self.ids.valueOfSmallWeapon.text = "Cost: " + str(self.manager.player.inventory["6"][1].value)
        self.ids.valueOfGeniusStrategy.text = "Cost: " + str(self.manager.player.inventory["7"][1].value)
        self.ids.valueOfBearArms.text = "Cost: " + str(self.manager.player.inventory["8"][1].value)
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)


    def updateGold(self):
        self.ids.playerGold.text = "Gold: " + str(self.manager.player.data_model.gold)
        pass

    def purchase(self):
        print("Item purchased.")

    def on_touch_move(self, touch):
        return

#class SoundBoard(Screen):
#    def shopSound(self):
#        self.shop_tune = SoundLoader.load("../Assets/Sound/Shop2.mp3")
#        if self.shop_tune:
#            self.shop_tune.play()
#    
#    def stopShop(self):
#        self.shop_tune.stop()

class House(Screen):
    def update(self):
        pass

    def on_pre_enter(self, *args):
        try:
            self.ids.critter_name.text = str(int(self.manager.critter.name))
        except:
            self.ids.critter_name.text = str(self.manager.critter.name)
        self.ids.critter_image.source = self.manager.critter.data_model.imgpath
        self.manager.critter.level = self.manager.critter.data_model.level
        self.manager.critter.hp = self.manager.critter.data_model.hp
        self.manager.critter.atk = self.manager.critter.data_model.atk
        self.ids.critter_hp.text = str(self.manager.critter.hp)
        self.ids.critter_atk.text = str(self.manager.critter.atk)
        self.ids.critter_lvl.text = str(self.manager.critter.level)
        self.ids.gold.text = str(self.manager.player.data_model.gold)
    
    def on_enter(self, *args):
        pass

    def evolve(self):
        self.manager.critter.evolve()
        self.manager.critter.imgpath = self.manager.critter.data_model.imgpath

        self.ids.critter_image.source = self.manager.critter.data_model.imgpath
        self.ids.critter_image.reload()

    def on_touch_move(self, touch):
        return

    def safeImageRender(self):
        try:
            return self.manager.critter.getImagepath()
        except:
            return "../Assets/Images/External/small_dog.jpg"

class CritterSelect(Screen):

    show_name_label = False
    chosen_critter = [Foxy, Snek, Birb]

    # def companionate(self, ide):
    #     nx = (self.width)/2 - 128
    #     # anim = Animation(x=nx) + Animation(size=(100, 100), duration=1.)
    #     # anim.start(self.ids[ide])
    #     self.ids[ide].x = nx

    def resolveCritter(self, ss):
        if (type(self.chosen_critter)==list):
            self.chosen_critter = self.chosen_critter[ss]
            return
        else:
            self.chosen_critter = [Foxy, Snek, Birb][ss]
            return -1

    def on_pre_enter(self, *args):
        self.hide_widget(ide="name_title")
        self.hide_widget(ide="name_menu")

    def hide_widget(self, ide, dohide=True):
        wid = self.ids[ide]
        if hasattr(wid, 'saved_attrs'):
            if not dohide:
                wid.height, wid.size_hint_y, wid.opacity, wid.disabled = wid.saved_attrs
                del wid.saved_attrs
        elif dohide:
            wid.saved_attrs = wid.height, wid.size_hint_y, wid.opacity, wid.disabled
            wid.height, wid.size_hint_y, wid.opacity, wid.disabled = 0, None, 0, True

    def on_leave(self, *args):
        self.hide_widget(ide="name_title", dohide=False)
        self.hide_widget(ide="fox_button", dohide=False)
        self.hide_widget(ide="fox_label", dohide=False)
        self.hide_widget(ide="bird_button", dohide=False)
        self.hide_widget(ide="bird_label", dohide=False)
        self.hide_widget(ide="snake_button", dohide=False)
        self.hide_widget(ide="snake_label", dohide=False)
        self.ids.name_field.text = ""

    def update(self):
            pass
    def on_touch_move(self, touch):
        return

class EnemySelect(Screen):  
    pass 

class Options(Screen):
    pass

class Resume(Screen):
    def on_pre_enter(self):
        try:
            with open(f"../Assets/Saves/State1/critter.desc", "r") as f:
                save_attributes = f.read().split(", ")
            with open(f"../Assets/Saves/State1/player.desc", "r") as f:
                save_attributes2 = f.read().split(", ")
            display_string = save_attributes[0] + \
                "; Lvl: " + save_attributes[1] + \
                save_attributes[2] + \
                    " Gold: " + save_attributes2[0]
            self.ids.save_slot_1.text = display_string
        except:
            print("No description file for slot one available.")
        try:
            with open(f"../Assets/Saves/State2/critter.desc", "r") as f:
                save_attributes = f.read().split(", ")
            with open(f"../Assets/Saves/State2/player.desc", "r") as f:
                save_attributes2 = f.read().split(", ")
            display_string = save_attributes[0] + \
                "; Lvl: " + save_attributes[1] + \
                save_attributes[2] + \
                    " Gold: " + save_attributes2[0]
            self.ids.save_slot_2.text = display_string
        except:
            print("No description file for slot one available.")
        try:
            with open(f"../Assets/Saves/State3/critter.desc", "r") as f:
                save_attributes = f.read().split(", ")
            with open(f"../Assets/Saves/State3/player.desc", "r") as f:
                save_attributes2 = f.read().split(", ")
            display_string = save_attributes[0] + \
                "; Lvl: " + save_attributes[1] + \
                save_attributes[2] + \
                    " Gold: " + save_attributes2[0]
            self.ids.save_slot_3.text = display_string
        except:
            print("No description file for slot one available.")

    def loadGame(self, nstate):
        try:
            nsaves = len(os.listdir("../Assets/Saves/"))
            if nsaves==0:
                print('No save states found.')
                return -1
            else:
                with open(f"../Assets/Saves/State{nstate}/critter.desc", "r") as f:
                    save_attributes = f.read().split(", ")
                with open(f"../Assets/Saves/State{nstate}/critter.json", "r") as f:
                    former = json.loads(f.read())
                    saver = {k: v for k, v in former.items() if v}
                with open(f"../Assets/Saves/State{nstate}/player.json", "r") as f:
                    pformer = json.loads(f.read())
                    psaver = {k: v for k, v in pformer.items()
                              if (v or v == 0)}
                if save_attributes[2] == "Bird":
                    self.manager.critter = self.manager.tfm(Birb(**saver))
                elif save_attributes[2] == "Snake":
                    self.manager.critter = self.manager.tfm(Snek(**saver))
                else:
                    self.manager.critter = self.manager.tfm(Foxy(**saver))
                self.manager.player = self.manager.tfm(Player(critter=self.manager.critter.data_model, **psaver))
                self.manager.critter.data_model.atk += self.manager.critter.data_model.level* \
                    self.manager.critter.data_model.growths["atk"]
                self.manager.critter.data_model.hp += self.manager.critter.data_model.level * \
                    self.manager.critter.data_model.growths["hp"]
                self.manager.critter.updateStats()
                return 0
        except Exception as e:
            print(e)
            print("Error reading save state from disk.")
            return -1

class Save(Screen):

    def on_pre_enter(self):
        try:
            with open(f"../Assets/Saves/State1/critter.desc", "r") as f:
                save_attributes = f.read().split(", ")
            with open(f"../Assets/Saves/State1/player.desc", "r") as f:
                save_attributes2 = f.read().split(", ")
            display_string = save_attributes[0] + \
                "; Lvl: " + save_attributes[1] + \
                save_attributes[2] + \
                " Gold: " + save_attributes2[0]
            self.ids.save_to_slot_1.text = display_string
        except:
            self.ids.save_to_slot_1.text = "No Save Data"
        try:
            with open(f"../Assets/Saves/State2/critter.desc", "r") as f:
                save_attributes = f.read().split(", ")
            with open(f"../Assets/Saves/State2/player.desc", "r") as f:
                save_attributes2 = f.read().split(", ")
            display_string = save_attributes[0] + \
                "; Lvl: " + save_attributes[1] + \
                save_attributes[2] + \
                " Gold: " + save_attributes2[0]
            self.ids.save_to_slot_2.text = display_string
        except:
            self.ids.save_to_slot_2.text = "No Save Data"
        try:
            with open(f"../Assets/Saves/State3/critter.desc", "r") as f:
                save_attributes = f.read().split(", ")
            with open(f"../Assets/Saves/State3/player.desc", "r") as f:
                save_attributes2 = f.read().split(", ")
            display_string = save_attributes[0] + \
                "; Lvl: " + save_attributes[1] + \
                save_attributes[2] + \
                " Gold: " + save_attributes2[0]
            self.ids.save_to_slot_3.text = display_string
        except:
            self.ids.save_to_slot_3.text = "No Save Data"

    def serialize(self, obj):
        saver = {}
        for i in [i for i in dir(obj) if i!="critter"][28:]:
            saver[i] = getattr(obj, i, None)
        ##Overwrite to remove the misses:
        saver = {k: v for k, v in saver.items() if v is not None}
        return saver

    def saveGame(self, nstate):
        if not os.path.isdir("../Assets/Saves/"):
            os.mkdir("../Assets/Saves/")
        if not os.path.isdir(f"../Assets/Saves/State{nstate}"):
            os.mkdir(f"../Assets/Saves/State{nstate}")
        ##Walk through and save each of the global objects:
        with open(f"../Assets/Saves/State{nstate}/critter.json", "w") as f:
            json.dump(self.manager.critter.data_model, f,
                      default=lambda x: self.serialize(x))
        with open(f"../Assets/Saves/State{nstate}/player.json", "w") as f:
            json.dump(self.manager.player.data_model, f,
                      default=lambda x: self.serialize(x))
        with open(f"../Assets/Saves/State{nstate}/critter.desc", "w") as f:
            f.write(f"{self.manager.critter.name}, {self.manager.critter.level}, {self.manager.critter.breed}")
        with open(f"../Assets/Saves/State{nstate}/player.desc", "w") as f:
            f.write(f"{self.manager.player.data_model.gold}")

class Savebeforeexiting(Screen):
    pass

class Credits(Screen):
    pass

##Main App Units:
class WindowManager(ScreenManager):
    tfm = PythonToKivy()
    soundboard = SoundBoard(library=library)

    def chooseCritter(self, customPyObj):
        self.critter = self.tfm(customPyObj)

    def createPlayer(self):
        playerPyObject = Player(critter=self.critter.data_model)
        self.player = self.tfm(playerPyObject)

    def createSlime(self):
        customPyObj = Competition(Critter=self.critter.data_model, Difficulty=0, Player=self.player)
        self.competition = self.tfm(customPyObj)

    def createOrc(self):
        customPyObj = Competition(Critter=self.critter.data_model, Difficulty=1, Player=self.player)
        self.competition = self.tfm(customPyObj)

    def createDragon(self):
        customPyObj = Competition(Critter=self.critter.data_model, Difficulty=2, Player=self.player)
        self.competition = self.tfm(customPyObj)

class CritterApp(App):
    """Main launch point for the Critter app."""
    def build(self):
        return
        # return kv


if __name__ == '__main__':
    # kv = Builder.load_file("./critter.kv")
    CritterApp().run()
