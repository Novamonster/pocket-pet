#!/usr/bin/env python3

##Imports --Py:
import re
from functools import wraps
from typing import Dict, List, Tuple, Union

##Imports --Internal:
from ttils import Normer

##Imports --Kivy:
import kivy
kivy.require('1.11.1')

from kivy.properties import DictProperty, ListProperty, NumericProperty, StringProperty
from kivy.event import EventDispatcher

def transformer(func):
    func._converter = True
    return func

class ConverterClass(type):
    '''Generic use-case type for a converter that applies transforms or validations.'''
    def __new__(cls, name, bases, kivd, **kwargs):
        kivd.update(kwargs)
        obj = type.__new__(cls, name, bases, kivd)
        obj._converters = [tfm for tfm in kivd.values() if hasattr(tfm, "_converter")]
        return obj

    @classmethod
    def transformer(cls, func):
        @wraps(func)
        def wrapper(cls, *args, **kwargs):
            func._converter = True
            result = func(cls, *args, **kwargs)
            return result
        return wrapper

class Converter(metaclass=ConverterClass):

    """
    Define object to be callable.
    """
    def introspect(cls, x: object):
    	return {i: getattr(x, f"{i}") for i in dir(x)}

    def __call__(cls, pyObj: object):
        _pyObj = pyObj

        for _converter in cls._converters:
            _pyObj = _converter(cls, _pyObj)
            # print("called", _converter, _converter.__name__)

        return _pyObj
    
    def __setattr__(cls, ref: str, new_val: object) -> int:
        cls[ref]  = new_val
        return 0

class kvObj(EventDispatcher):

    def __init__(self, data_ref: object, _pyObj_attributes: List[str]):
        super(kvObj, self).__init__()
        self.data_model = data_ref
        self._pyObj_attributes = _pyObj_attributes
        # print(self.__getattribute__)
        # print(object.__getattribute__(self, "_pyObj_attributes"))

    # def __getattribute__(self, key):
    #     if key in self._pyObj_attributes:
    #         return self.__dict__[key]
    #     else:
    #         object.__getattribute__(self, key)


    # def __getattribute__(self, key):
    #     if key == '_pyObj_attributes':
    #         return object.__getattribute__(self, key)
    #     elif key in self._pyObj_attributes:
    #         #Get and overwrite from the Python model:
    #         val = str(getattr(self.data_model, key))
    #         setattr(self, key, val)
    #         #Proceed as normal
    #         return EventDispatcher.__getattribute__(self, key)
    #     else:
    #         return EventDispatcher.__getattribute__(self, key)

    def update(self, func):
        """Decorator to put around any UI function that update the common
           data model"""
        @wraps(func)
        def wrapper(*args, **kwargs):
            ret = func(*args, **kwargs)
            # Update UI Data Model wrapper
            for attr in self._pyObj_attributes:
                getattr(self, attr)
            return ret
        return wrapper

class PythonToKivy(Converter):

    """
    A callable class that converts python objects to Kivy compatible objects.
    """

    @transformer
    def toKivy(self, pyObj: object):
        '''Depends on Normer for type identification.'''
        tester = Normer()
        ####STILL DEPENDS ON HAVING ALL ATTRIBUTES IN THE DATA
        if not hasattr(pyObj, "all_attributes"):
        	setattr(pyObj, "all_attributes", self.introspect(pyObj))
        kivd, converted = pyObj.all_attributes, []
        KivyObject = kvObj(data_ref = pyObj, _pyObj_attributes = converted)

        for attr,v in kivd.items():
            ##Catch the protected attributes or those that are Kivy keywords.
            if attr.startswith('_'):
                continue
            if attr in dir(EventDispatcher()):
                continue

            ##Log and convert.
            KivyObject._pyObj_attributes.append(attr)
            typ = tester.find_dtype(value=v)
            if typ in tester.numeric:
                KivyObject.apply_property(**{attr:NumericProperty(v)})
                ##Removed as Kivy's type processing may be different from Normer()
                # KivyObject.create_property(attr)
                # setattr(KivyObject, attr, v)
                continue
            elif typ in tester.text:
                KivyObject.apply_property(**{attr:StringProperty(v)})
                continue
            else:
                if typ==list:
                    KivyObject.apply_property(**{attr:ListProperty(v)})
                    continue
                if typ==dict:
                    KivyObject.apply_property(**{attr:DictProperty(v)})
                    continue
                if typ==set or typ==tuple:
                    KivyObject.apply_property(**{attr:ListProperty([i for i in v])})
                    continue
                print("Err converting", KivyObject._pyObj_attributes.pop(attr))
        # print(KivyObject._pyObj_attributes)
        #KivyObject.update({"_pyObj_attributes":converted})
        return KivyObject
