library = {
		"buttonpress": "../Assets/Sound/Button_pressing.wav",
		"special": "../Assets/Sound/Special.mp3",
		"house": "../Assets/Sound/House.mp3",
		"shop": "../Assets/Sound/Shop.mp3",
		"inventory": "../Assets/Sound/Inventory.wav",
		"explore": "../Assets/Sound/Competition.wav",
		"slime": "../Assets/Sound/Slime.wav",
		"orc": "../Assets/Sound/Orc.wav",
		"dragon": "../Assets/Sound/Dragon.wav",
		"critterpunch": "../Assets/Sound/Critter_punch.wav",
		"enemypunch": "../Assets/Sound/Enemy_punch.wav",
		"victory": "../Assets/Sound/Victory.wav",
		"defeat": "../Assets/Sound/Defeat.wav",
		"mainmenu": "../Assets/Sound/Main_menu.wav",
		"critterselect": "../Assets/Sound/Critter_select.wav"
	}