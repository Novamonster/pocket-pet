#!/usr/bin/env python3

##Imports --Py:
import re
from functools import wraps
from typing import Dict, List, Tuple, Union

##################
# Utility
##################

class Normer:
    numeric = [int, float, complex]
    data = [dict, list, set, tuple]
    text = [str]

    def __init__(self, column: Union[List[str], str] = "") -> None:
        self.column = column
        self.hasdigit = re.compile(".*\\d.*").match
        self.isint = re.compile(r"^[0-9]+$").match
        self.isfloat = re.compile(r"^\d+(\.\d*)?|\.\d+$").match
        self.removespace = lambda x: re.sub(r"^\s+|\s+$", "", x)
        self.var_type = self.find_dtype(self.column)
    
    def find_dtype(self, value: Union[str, Dict, List, Tuple]) -> Union[int, float, str]:
        if type(value)==dict:
            return dict
        if type(value)==list:
            return list
        if type(value)==tuple:
            return tuple
        try:
            valor = self.purify_str(value)
        except:
            raise Exception("Could not recognize data type.")
        if self.contains_numeric(valor):
            return self.type_of_numeric(valor)
        else:
            return str 
        
    def contains_numeric(self, value: str) -> bool:
        return bool(self.hasdigit(value))
    
    def type_of_numeric(self, value: str) -> Union[int, float, str]:
        if bool(self.isint(value)):
            return int
        else:
            if bool(self.isfloat(value)):
                return float
            else:
                return str ###This is the varchar option
    
    def purify_str(self, column: str) -> str:
        return self.removespace(str(column))
    
    def sort_function(self, column: str) -> str:
        pass